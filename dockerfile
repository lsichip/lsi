FROM node

RUN mkdir /lsi

WORKDIR /lsi
COPY package.json /lsi
RUN yarn install

COPY . /lsi

RUN yarn build

CMD yarn start

EXPOSE 3000